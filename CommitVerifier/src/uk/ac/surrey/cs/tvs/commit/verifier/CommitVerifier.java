/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.commit.verifier;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;
import it.unisa.dia.gas.plaf.jpbc.pairing.DefaultCurveParameters;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A single class that provides an example implementation of a commit signature verifier. We have cut down the code to the minimum
 * to show the core functionality.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitVerifier {

  /**
   * Reads a file into a Message Digest
   * 
   * @param file
   *          File to read into the digest
   * @param md
   *          MessageDigest to read file into
   * @return int of the number of bytes read
   * @throws IOException
   */
  public static int addFileIntoDigest(File file, MessageDigest md) throws IOException {
    int filesize = 0;
    int read = 0;
    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
    DigestInputStream dis = new DigestInputStream(bis, md);
    byte[] bytesIn = new byte[1024];

    try {
      while ((read = dis.read(bytesIn)) != -1) {
        filesize = filesize + read;
      }
    }
    finally {
      dis.close();
    }

    return filesize;
  }

  /**
   * Checks if a Commit Description exists in the commitTime string. The commitTime will contain a concatenation of both the
   * commitTime and commitDescription when an extra commit is performed. This checks the length of the string and if appropriate,
   * splits the string to return just the commitDescription.
   * 
   * @param commitTime
   *          String of the commitTime to check
   * @return String containing the commitDescription, or null if no commitDescription was included
   */
  public static String getCommitDesc(String commitTime) {
    if (commitTime.length() == 13) {
      return null;
    }
    else {
      return commitTime.substring(13);
    }
  }

  /**
   * Checks if a Commit Description exists in the commitTime string. The commitTime will contain a concatenation of both the
   * commitTime and commitDescription when an extra commit is performed. This checks the length of the string and if appropriate,
   * splits the string to return just the commitTime.
   * 
   * @param commitTime
   *          String of the commitTime to check
   * @return String containing the commitTime
   */
  public static String getCommitTime(String commitTime) {
    if (commitTime.length() == 13) {
      return commitTime;
    }
    else {
      return commitTime.substring(0, 13);
    }
  }

  /**
   * Main method to run the application
   * 
   * @param args
   *          String array of arguments
   */
  public static void main(String[] args) {
    if (args.length == 2) {
      CommitVerifier commitVerifier = new CommitVerifier(args[1]);
      commitVerifier.loadKey("jointkey.json");
      commitVerifier.verifyAll(args[0]);

    }
    else if (args.length == 3) {
      CommitVerifier commitVerifier = new CommitVerifier(args[1]);
      commitVerifier.loadKey(args[2]);
      commitVerifier.verifyAll(args[0]);
    }
    else {
      printUsage();
    }

  }

  /**
   * Prints usage instructions
   */
  public static void printUsage() {
    System.out.println("CommitVerifier");
    System.out.println("CommitVerifier is an example implementation of a Commit Verifier");
    System.out.println("for the SuVote system. It can be used to verify the BLS digital");
    System.out.println("signatures included with the commits");
    System.out.println();
    System.out.println("Usage:");
    System.out.println("  java -jar CommitVerifier.jar folderOfCommits unzipDirectory [publicKeyFile]");
    System.out.println("    folderOfCommits: path to the folder containing the commit files");
    System.out.println("    unzipDirectory: path to a folder to unzip commits to");
    System.out.println("    publicKeyFile: path to JSON file containing public key (optional)");

  }

  /**
   * Curve Parameters used for signature verification
   */
  private DefaultCurveParameters curveParams;

  /**
   * BLS Element that holds the g value
   */
  private Element                g;

  /**
   * Pairing that we use for signature verification
   */
  private Pairing                pairing;

  /**
   * BLS Element that holds the public key
   */
  private Element                publicKey;

  /**
   * A directory to unzip files to
   */
  private File                   unzipdir;

  /**
   * Constructor for the CommitVerifier
   * 
   * @param unzipDirPath
   *          String path to the directory to unzip commit attachments to
   */
  public CommitVerifier(String unzipDirPath) {
    System.out.println("Creating unzip directory at:" + unzipDirPath);
    // Create the unzip direcotry
    this.unzipdir = new File(unzipDirPath);
    if (!this.unzipdir.exists()) {
      this.unzipdir.mkdirs();
    }

    // Load the curve parameters from the file
    System.out.println("Loading curve params");
    InputStream is = null;
    try {
      is = this.getClass().getResourceAsStream("d62003-159-158.param");
      this.curveParams = new DefaultCurveParameters().load(is);
    }
    finally {
      if (is != null) {
        try {
          is.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    // Create the pairing object
    System.out.println("About to create pairing");
    this.pairing = PairingFactory.getPairing(this.curveParams);
    System.out.println("Finished creating pairing");
  }

  /**
   * Gets the appropriate internalSignableContent for different message types.
   * 
   * @param msg
   *          JSONObject containing a valid JSON message object
   * @return String of the relevant internal signable content
   * @throws JSONException
   */
  public String getInternalSignableContent(JSONObject msg) throws JSONException {
    StringBuffer buf = new StringBuffer();
    switch (msg.getString("type")) {
      case "audit":
        buf.append("audit");
        buf.append(msg.getString("serialNo"));
        buf.append(msg.getString("commitTime"));
        buf.append(msg.getString("_reducedPerms"));
        break;
      case "vote":
        buf.append(msg.getString("serialNo"));
        buf.append(msg.getString("district"));
        buf.append(msg.getString("_vPrefs"));
        buf.append(msg.getString("boothSig"));
        buf.append(msg.getString("boothID"));
        buf.append(msg.getString("commitTime"));
        break;
      case "pod":
        buf.append(msg.getString("serialNo"));
        buf.append(msg.getString("district"));
        buf.append(msg.getJSONArray("ballotReductions").toString());
        buf.append(msg.getString("commitTime"));
        break;
      case "ballotauditcommit":
      case "ballotgencommit":
      case "file":
        buf.append(msg.getString("submissionID"));
        buf.append(msg.getString("_digest"));
        buf.append(msg.getString("boothID"));
        buf.append(msg.getString("commitTime"));
        break;
      case "mixrandomcommit":
        buf.append(msg.getString("submissionID"));
        buf.append(msg.getString("boothID"));
        buf.append(msg.getString("printerID"));
        buf.append(msg.getString("_digest"));
        buf.append(msg.getString("commitTime"));
        break;
      case "cancel":
        buf.append("cancel");
        buf.append(msg.getString("serialNo"));
        break;
      default:
        System.out.println("Unknown type:" + msg.getString("type"));

    }
    return buf.toString();

  }

  /**
   * Utility method to load a JSONObject from a file
   * 
   * @param file
   *          String path to file containing a JSON Object
   * @return JSONObject representing file contents
   */
  public JSONObject loadJSONObjectFromFile(String file) {
    BufferedReader br = null;
    String line = null;
    try {
      // Read entire file into a buffer
      br = new BufferedReader(new FileReader(file));
      StringBuffer sb = new StringBuffer();
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }
      // convert and return JSONObject
      return new JSONObject(sb.toString());

    }
    catch (JSONException | IOException e) {
      e.printStackTrace();
    }
    finally {
      if (br != null) {
        try {
          br.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  /**
   * Utility method to load the BLS Public key into the element variables for use when verifying commit files.
   * 
   * @param keyFile
   *          String path to the public key JSON file
   */
  public void loadKey(String keyFile) {
    try {
      JSONObject keyObject = this.loadJSONObjectFromFile(keyFile);
      this.publicKey = this.pairing.getG2().newElement();
      JSONObject wbbKey = keyObject.getJSONObject("WBB").getJSONObject("pubKeyEntry");
      this.publicKey.setFromBytes(Base64.decodeBase64(wbbKey.getString("publicKey")));
      this.g = this.pairing.getG2().newElement();
      // Set the element value from the bytes decoded from the JSON
      this.g.setFromBytes(Base64.decodeBase64(wbbKey.getString("g")));
    }
    catch (JSONException e) {
      e.printStackTrace();
    }

  }

  /**
   * Utility method for unzipping a zip file to a target directory. Note: for simplification we are no checking max size or
   * filenames for validity. See ZipUtil in the vVoteLibrary project for a more thorough implementation that protects against
   * malicious zip files.
   * 
   * @param filepath
   *          String path to zip file to unzip
   * @param targetDirectory
   *          File pointing to directory to unzip to
   * @throws IOException
   */
  public final void unzip(String filepath, File targetDirectory) throws IOException {
    System.out.println("\tStarting unzipping of " + filepath);
    FileInputStream fis = new FileInputStream(filepath);
    ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
    ZipEntry entry;

    try {
      // Loop through each zip entry
      while ((entry = zis.getNextEntry()) != null) {
        System.out.println("\tUnzipping: " + entry.getName());
        int count;
        byte data[] = new byte[1024];
        String name = entry.getName();
        FileOutputStream fos = new FileOutputStream(new File(targetDirectory, name));
        BufferedOutputStream dest = null;
        try {
          dest = new BufferedOutputStream(fos, 1024);

          // Read data until max zip entry. If it exceeds stop and throw an exception
          while ((count = zis.read(data, 0, 1024)) != -1) {
            dest.write(data, 0, count);
          }
          dest.flush();
        }
        finally {
          if (dest != null) {
            dest.close();
          }
        }
        zis.closeEntry();

      }
    }
    finally {
      zis.close();
    }
    System.out.println("\tFinished unzipping:" + filepath);
  }

  /**
   * Verifies a single commit signature file by reading in the commit data and any associated attachments.
   * 
   * @param commitSignaturePath
   *          String path to the commit signature JSON file
   * @return boolean value, true if the signature verifies, false if it does not
   * @throws IOException
   * @throws NoSuchAlgorithmException
   * @throws JSONException
   */
  public boolean verify(String commitSignaturePath) throws IOException, NoSuchAlgorithmException, JSONException {
    BufferedReader br = null;

    try {
      // Read the signature file
      File commitSignatureFile = new File(commitSignaturePath);
      File parentDir = new File(commitSignatureFile.getCanonicalPath()).getParentFile();
      JSONObject signatureFile = this.loadJSONObjectFromFile(commitSignatureFile.getAbsolutePath());
      System.out.println("\tLoaded signature file");

      // Create a signature digest
      MessageDigest sigDigest = MessageDigest.getInstance("SHA1");
      sigDigest.update("Commit".getBytes());
      sigDigest.update(CommitVerifier.getCommitTime(signatureFile.getString("commitTime")).getBytes());

      // Create a commit digest to contain the messages and attachments
      MessageDigest commitDigest = MessageDigest.getInstance("SHA1");
      br = new BufferedReader(new FileReader(new File(parentDir, signatureFile.getString("commitTime") + ".json")));
      String line = null;

      // Create an output directory to unzip the attachments to
      File outputDir = new File(this.unzipdir, signatureFile.getString("commitTime") + "/");
      if (!outputDir.exists()) {
        outputDir.mkdirs();
      }
      this.unzip(parentDir + "/" + signatureFile.getString("commitTime") + "_attachments.zip", outputDir);
      System.out.println("\tCreating Commit Digest");

      // Read the commit messages and process them
      while ((line = br.readLine()) != null) {
        JSONObject msg = new JSONObject(line);
        commitDigest.update(this.getInternalSignableContent(msg).getBytes());
        if (msg.has("_fileName")) {
          addFileIntoDigest(new File(outputDir + "/" + msg.getString("_fileName")), commitDigest);
        }
      }
      System.out.println("\tFinished Creating Commit Digest");
      byte[] digest = commitDigest.digest();

      // Complete the signature digest and verify it
      sigDigest.update(digest);
      if (getCommitDesc(signatureFile.getString("commitTime")) != null) {
        sigDigest.update(getCommitDesc(signatureFile.getString("commitTime")).getBytes());
      }
      boolean result = this.verifySignature(sigDigest.digest(), signatureFile.getString("jointSig"));
      System.out.println("\tVerified:" + result);
      return result;
    }
    finally {
      if (br != null) {
        br.close();
      }
    }
  }

  /**
   * Loops through all files in the specified folder looking for commit signature files, which are distinguished by the suffix
   * "_signature.json". For each signature file found it will attempt to verify the commit covered by that signature.
   * 
   * @param folder
   *          String containing path to folder of commits to verify
   */
  public void verifyAll(String folder) {
    HashMap<String, Boolean> verificationResults = new HashMap<String, Boolean>();
    File baseDir = new File(folder);
    File[] files = baseDir.listFiles();
    for (File cFile : files) {
      if (cFile.getName().endsWith("_signature.json")) {
        try {
          System.out.println("Starting verification of:" + cFile.getName());
          verificationResults.put(cFile.getName(), this.verify(cFile.getAbsolutePath()));
          System.out.println("Finished verification of:" + cFile.getName());
        }
        catch (NoSuchAlgorithmException | IOException | JSONException e) {
          e.printStackTrace();
        }
      }
    }
    System.out.println("====================================================");
    System.out.println("==             Verification Complete              ==");
    System.out.println("==                    Results                     ==");
    System.out.println("====================================================");
    Iterator<String> filesVerified = verificationResults.keySet().iterator();
    while (filesVerified.hasNext()) {
      String fileName = filesVerified.next();
      System.out.println(fileName + " verified:" + verificationResults.get(fileName));
    }
  }

  /**
   * Utility method for verifying a BLS signature
   * 
   * @param hash
   *          byte array containing the signature hash (digest)
   * @param signatureString
   *          String containing a base64 encoded copy of the actual signature
   * @return boolean, true if valid, false if not
   */
  public boolean verifySignature(byte[] hash, String signatureString) {
    Element signature = this.pairing.getG1().newElement();
    signature.setFromBytes(Base64.decodeBase64(signatureString));

    // Map the hash onto an element in G1
    Element h = this.pairing.getG1().newElement().setFromHash(hash, 0, hash.length).getImmutable();

    // Create the signature pairing
    Element sigPairing = this.pairing.pairing(signature, this.g);

    // Create the hash pairing
    Element hashPairing = this.pairing.pairing(h, this.publicKey);

    // If the pairing are equal the signature is valid
    return sigPairing.isEqual(hashPairing);
  }

}
