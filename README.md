# README #

Commit Verifier provides an example implementation for verifying the commit signatures.

Download Compiled Java: [Release](https://bitbucket.org/tvsproject/suvote_commitverifier/downloads/commitVerifier.zip) Unzip to a directory and follow the usage instructions.
## CommitVerifier ##

CommitVerifier is an example implementation of a Commit Verifier for the SuVote system. It can be used to verify the BLS digital signatures include with the commits

### Usage: ###
- java -jar CommitVerifier.jar folderOfCommits unzipDirectory [publicKeyFile]
- Usage:
    - folderOfCommits: path to the folder containing the commit files
    - unzipDirectory: path to a folder to unzip commits to
    - publicKeyFile: path to JSON file containing public key (optional)